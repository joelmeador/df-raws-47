# df-raws-47

A place to track changes to raws for a community DF game

## Tools
* [Weird Text Format Tools](http://dffd.bay12games.com/file.php?id=4175) for working with announcements files - see [DF2014:Dipscript](https://www.dwarffortresswiki.org/index.php/DF2014:Dipscript)

## Previous Kitfox Succession Games
* [siegedcraft](https://justpaste.it/siegedcraft)
